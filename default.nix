let
  sources = import ./nix/sources.nix {};

  haskellNix = import sources.haskellNix {};

  nixpkgs = import
    haskellNix.sources.nixpkgs-unstable
    haskellNix.nixpkgsArgs;

  gitignore-src = import sources.gitignore-src {};
in

with gitignore-src;
with nixpkgs;

rec {
  blogbuster = haskell-nix.project {
    src = haskell-nix.haskellLib.cleanGit {
      name = "blogbuster";
      src = gitignoreSource ./.;
    };

    compiler-nix-name = "ghc925";
  };

  blogbuster-shell = blogbuster.shellFor {
    withHoogle = true;

    tools = {
      cabal = "3.6.2.0";
      hlint = "latest";
      # stylish-haskell = "latest";
    };

    buildInputs = [
      git
    ];

    exactDeps = true;
  };
}
