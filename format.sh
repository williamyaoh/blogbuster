#!/usr/bin/env sh

# Apply code formatting to all source files.

find src/ -type f -name "*.hs" -print -exec stylish-haskell -i {} \;
