{-# LANGUAGE OverloadedStrings #-}

module Blogbuster.Content.Footnotes where

import Data.Char ( isAlphaNum )
import Data.Text ( Text )
import qualified Data.Text.Encoding as T
import Data.ByteString ( ByteString )
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as ASCII
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Builder as B
import qualified Data.ByteString.Internal as B
import Data.HashMap.Strict ( HashMap )
import qualified Data.HashMap.Strict as Map

import Control.Arrow ( first )

-- You know what would be really cool? A way to use attoparsec parsers
-- as lenses. Because we seem to really commonly need a way to simply
-- pull out every instance of a matching parse and then insert something
-- in the 'hole' that's created.
-- The type would be something like...
-- `(a -> ByteString) -> Parser a -> Traversal ByteString a`
-- i.e. we basically provide the information needed to turn the data
-- back in the right way. Since parsers only go one way...
-- The other option would be some kind of bidirectional parser.

addLinks :: (ByteString, ByteString) -> (ByteString, ByteString)
addLinks (body, footnotes) =
  let (footnoteB, lookup) = addFootnoteLinks Map.empty footnotes
      bodyB = addBodyLinks lookup body
  in (LB.toStrict (B.toLazyByteString bodyB), LB.toStrict (B.toLazyByteString footnoteB))
  where addFootnoteLinks :: HashMap Text Int -> ByteString -> (B.Builder, HashMap Text Int)
        addFootnoteLinks lookup b = if B.length b <= 2
          then (B.byteString b, lookup)
          else case (B.index b 0, B.index b 1) of
                 (0x25, 0x25) ->  -- ('%', '%')
                   let (note, bytelen) = noteName (B.drop 2 b)
                       tag = ( "<sup><a id=\""
                            <> B.byteString (T.encodeUtf8 $ note <> "-foot")
                            <> "\" href=\"#"
                            <> B.byteString (T.encodeUtf8 $ note <> "-body")
                            <> "\">"
                            <> B.byteString (T.encodeUtf8 "↥")
                            <> B.byteString (ASCII.pack (show $ Map.size lookup + 1))
                            <> "</a></sup>"
                             )
                   in first (tag <>) $
                        addFootnoteLinks
                          (Map.insert note (Map.size lookup + 1) lookup)
                          (B.drop (2 + bytelen) b)
                 (byte, _) -> first (B.word8 byte <>) $
                   addFootnoteLinks lookup (B.drop 1 b)

        addBodyLinks :: HashMap Text Int -> ByteString -> B.Builder
        addBodyLinks lookup b = if B.length b <= 2
          then B.byteString b
          else case (B.index b 0, B.index b 1) of
                 (0x25, 0x25) ->  -- ('%', '%')
                   let (note, bytelen) = noteName (B.drop 2 b)
                       tag = ( "<sup><a id=\""
                            <> B.byteString (T.encodeUtf8 $ note <> "-body")
                            <> "\" href=\"#"
                            <> B.byteString (T.encodeUtf8 $ note <> "-foot")
                            <> "\">"
                            <> B.byteString (ASCII.pack (show $ lookup Map.! note))
                            <> "</a></sup>"
                             )
                   in tag <> addBodyLinks lookup (B.drop (2 + bytelen) b)
                 (byte, _) -> B.word8 byte <> addBodyLinks lookup (B.drop 1 b)

noteName :: ByteString -> (Text, Int)
noteName b =
  let bytes = B.takeWhile (\b -> let c = B.w2c b in isAlphaNum c || c == '-') b
  in (T.decodeUtf8 bytes, B.length bytes)
