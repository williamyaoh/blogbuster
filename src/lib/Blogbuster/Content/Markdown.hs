{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Blogbuster.Content.Markdown where

import Control.Monad.IO.Class ( MonadIO, liftIO )

import Data.ByteString ( ByteString )
import qualified Data.ByteString.Lazy as LB
import Data.Text ( Text )
import Data.Text.Conversions

import Text.Pandoc
import qualified Text.Blaze.Renderer.Utf8 as Blaze

defaultReaderOpts :: ReaderOptions
defaultReaderOpts = def
  { readerExtensions = enableExtension Ext_smart pandocExtensions }

markdownToHTML5 :: MonadIO m => ByteString -> m ByteString
markdownToHTML5 = markdownToHTML5With defaultReaderOpts def

markdownToHTML5With
  :: MonadIO m
  => ReaderOptions
  -> WriterOptions
  -> ByteString
  -> m ByteString
markdownToHTML5With readerOpts writerOpts contents =
  liftIO $ do
    text :: Text <- decodeConvertText' (UTF8 contents)
    runIOorExplode $ do
      pandoc <- readMarkdown readerOpts text
      html   <- writeHtml5 writerOpts pandoc
      pure $! LB.toStrict $ Blaze.renderMarkup html

decodeConvertText'
  :: (MonadFail m, DecodeText Maybe a, FromText b)
  => a
  -> m b
decodeConvertText' input = case decodeConvertText input of
  Nothing -> fail "failed to decode input"
  Just output -> pure $! output
