{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Blogbuster.Content.Sections where

import Prelude hiding ( fail )

import Data.List ( find )
import Data.Char
import Data.Text as T hiding ( find )
import Data.ByteString as B hiding ( find )
import Data.Attoparsec.ByteString
import Data.Attoparsec.Combinator ( lookAhead )
import Data.Yaml ( decodeThrow )
import Data.Maybe ( fromMaybe )

import Control.Monad ( void, fail )
import Control.Applicative ( (<|>) )

import Freer

import Blogbuster.Meta

data Section = Section
  { sectionName :: Text
  , sectionContents :: ByteString
  }
  deriving (Eq, Show)

-- |
-- Parse the input document into sections.
sectionsP :: Parser [Section]
sectionsP = many' sectionP

sectionP :: Parser Section
sectionP = do
  sepName <- separatorP
  contents <- manyTill' anyWord8 (void (lookAhead separatorP) <|> endOfInput)
  pure (Section sepName (B.pack contents))

separatorP :: Parser Text
separatorP =
  string "===== " *> identifierP <* string " ==========" <* many' (satisfy (== 0x3D))

identifierP :: Parser Text
identifierP = T.pack <$> many1 idCharP

-- filterParse :: (a -> Bool) -> Parser a -> Parser a
-- filterParse f parse = do
--   result <- parse
--   if f result
--     then pure result
--     else fail "did not satisfy predicate"

idCharP :: Parser Char
idCharP = satisfyWith (toEnum . fromIntegral) (\c -> isAlphaNum c || c == '-')

-- asciiChar :: Parser Char
-- asciiChar = toEnum . fromIntegral <$>
--   satisfy (\byte -> not (testBit byte 7))

--------------------

data SectionParserF a where
  ParseSection :: Text -> SectionParserF ByteString
  ParseOptional :: Text -> SectionParserF (Maybe ByteString)
type SectionParser a = Freer SectionParserF a

section :: Text -> SectionParser ByteString
section = send . ParseSection

optionalSection :: Text -> SectionParser (Maybe ByteString)
optionalSection = send . ParseOptional

parseSections :: [Section] -> SectionParser a -> Either String a
parseSections sections = interpret $ \case
  ParseSection name -> case find ((== name) . sectionName) sections of
    Nothing -> Left ("could not find section " ++ T.unpack name)
    Just sec -> Right (sectionContents sec)
  ParseOptional name -> case find ((== name) . sectionName) sections of
    Nothing -> Right Nothing
    Just sec -> Right (Just $ sectionContents sec)

validateContents :: ByteString -> SectionParser a -> Either String a
validateContents contents parser =
  parseOnly (sectionsP <* endOfInput) contents >>= flip parseSections parser

validateM :: MonadFail m => ByteString -> SectionParser a -> m a
validateM contents parser = case validateContents contents parser of
  Left error -> fail error
  Right result -> pure result

--------------------

-- We should really fix the implementation of SectionParser... somewhere, to
-- where we can actually throw errors from within the parsing functions.
-- That would probably mean changing it into some more featureful monad,
-- ah well...

post :: SectionParser (ByteString, Value, Maybe ByteString)
post = (,,)
  <$> section "body"
  <*> fmap parseMeta (section "meta")
  <*> optionalSection "footnotes"
  where parseMeta :: ByteString -> Value
        parseMeta bs = fromMaybe mempty (fromJSON <$> decodeThrow bs)
