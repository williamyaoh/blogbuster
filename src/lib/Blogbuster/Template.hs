module Blogbuster.Template where

import Path

import Data.ByteString ( ByteString )
import qualified Text.StringTemplate as ST
import Data.Text.Conversions
import Data.Maybe ( fromMaybe )

type Templates = ST.STGroup ByteString

compileTemplates :: Path rel Dir -> IO (ST.STGroup ByteString)
compileTemplates = ST.directoryGroup . toFilePath

selfTemplate :: ST.STGroup ByteString -> ByteString -> ByteString
selfTemplate grp contents =
  let mtemplated = do
        strContents <- decodeConvertText (UTF8 contents)
        -- Technically this could break if the user creates a
        -- template called __main.st. But I don't expect that to happen.
        main <- pure $! ST.groupStringTemplates $
          [("__main", ST.newSTMP strContents)]
        withContext <- ST.getStringTemplate "__main" (ST.addSuperGroup main grp)
        pure $! ST.render withContext
  in fromMaybe contents mtemplated

applyTemplate :: ST.STGroup ByteString -> String -> ByteString -> ByteString
applyTemplate grp tempName contents =
  let mtemplated = do
        main <- ST.getStringTemplate tempName grp
        pure $! ST.render $
          ST.setAttribute "body" contents main
  in fromMaybe contents mtemplated
