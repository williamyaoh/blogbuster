{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE BangPatterns  #-}

-- |
-- This module is a bit of a crutch, where we need a lot of small data
-- structures that contain more than just a file path, but which need
-- to act as if they only contain a file path for the purposes of set
-- membership/hashing.
--
-- We should potentially look towards making TreeStore work using two
-- layers of maps, instead of making the "filepath" layer a Set, to
-- remove the need for something like this.

module Blogbuster.FileData
  ( FileData(..), splitDirs )
where

import Path

newtype FileData v = FileData (Path Rel File, v)
  deriving (Show, Functor)

instance Eq (FileData v) where
  (==) (FileData (f1, _)) (FileData (f2, _)) = f1 == f2
instance Ord (FileData v) where
  compare (FileData (f1, _)) (FileData (f2, _)) = f1 `compare` f2

splitDirs :: Path Rel Dir -> [Path Rel Dir]
splitDirs = go []
  where go !acc p =
          if p == parent p then acc
          else go (dirname p:acc) (parent p)
