{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE RankNTypes    #-}

module Blogbuster.Blog where

import Path

import Control.Applicative ( liftA2 )
import Control.Monad       ( ap )

import TreeStore

import Blogbuster.FileData
import Blogbuster.Mapping ( HasMapping(..), Production(..), normalizeProduction )
import Blogbuster.Output  ( OutputTree, produceOutput' )
import Blogbuster.Meta

data Blog' m a = Blog
  { blogF      :: Filesystem -> OutputTree m
  , blogResult :: a
  }
  deriving Functor
type Blog = Blog' IO ()

instance Applicative (Blog' m) where
  pure x = Blog { blogF = const mempty, blogResult = x }
  (<*>) = ap

instance Monad (Blog' m) where
  return = pure
  (>>=) (Blog blogF1 x) f = case f x of
    Blog blogF2 y ->
      Blog { blogF = liftA2 (<>) blogF1 blogF2, blogResult = y }

produce :: forall blogspec m a. HasMapping blogspec
        => Production blogspec m a
        -> Blog' m ()
produce (P (prod, _)) = Blog { blogF = normalizeProduction prod, blogResult = () }

blogbuster :: Path irel Dir -> Path orel Dir -> Blog -> IO (TreeStore (Path Rel Dir) (FileData Value))
blogbuster = blogbuster' id

blogbuster' :: (forall a. m a -> IO a)
            -> Path irel Dir
            -> Path orel Dir
            -> Blog' m a
            -> IO (TreeStore (Path Rel Dir) (FileData Value))
blogbuster' trans indir outdir (Blog blogF _) = do
  fs <- readFilesystem indir
  produceOutput' trans indir outdir (blogF fs)
