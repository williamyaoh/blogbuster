{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds      #-}
{-# LANGUAGE TypeOperators  #-}

-- |
-- Note that unless specified otherwise, all components require at least
-- one matched directory/file.
--
-- The types are currently fully uppercase in order to work around an
-- issue with the @path@ library.

module Blogbuster.Primitive where

import Data.Kind    ( Type )
import GHC.TypeLits ( Symbol )

-- |
-- Specifies that the production should descend into any directories
-- matching the specified glob, and provide the name of any matched
-- directories as parameters to production functions.
--
-- Note that if you just want to descend into directories without
-- capturing the directory name, use a Symbol using -XDataKinds.
data DIR (selector :: Symbol)
-- |
-- Match any files using the given glob, and register their output mapping.
data FILE (selector :: Symbol)
data FILES (selectors :: [Symbol])

-- |
-- Allows the contained matcher to still succeed
-- even if no files/directories were matched.
data OPT (matcher :: Type)

infixr 7 :/
infixr 9 :+

-- |
-- Path separator for descending into directories.
data (:/) (a :: k) (b :: Type)
-- |
-- Combines matchers looking for objects in the same directory.
data (:+) (a :: Type) (b :: Type) = a :+ b
