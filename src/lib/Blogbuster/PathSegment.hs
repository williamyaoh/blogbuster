{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}

module Blogbuster.PathSegment where

import Path

import qualified Data.Aeson.KeyMap as JSON
import Data.Foldable
import Data.Time
import Data.String.Interpolate ( i )
import Data.Proxy ( Proxy(..) )
import Data.Validation ( validationNel )

import GHC.TypeLits ( KnownSymbol, symbolVal )

import Glob
import TreeStore
import Blogbuster.FileData
import Blogbuster.Primitive
import Blogbuster.Meta
import Blogbuster.Mapping

data DATE (spec :: k)

-- Do we actually want to add in the parameter for the date into the
-- mapping functions? For now, I kind of don't feel like doing that, so
-- we won't.

instance (HasMapping blogspec, KnownSymbol selector)
    => HasMapping ((DATE (DIR selector)) :/ blogspec) where
  type NameMapper (DATE (DIR selector) :/ blogspec) =
    NameMapper (DIR selector :/ blogspec)
  type ContentMapper (DATE (DIR selector) :/ blogspec) m =
    ContentMapper (DIR selector :/ blogspec) m

  mapBlog nameF contentF fs =
    let glob = Glob.parse (symbolVal @selector Proxy)
        toMappings dir sub =
          singletonFolder dir $
            case toDate dir of
              Nothing -> singletonValue $ validationNel $ Left
                [i|Couldn't parse a valid date from folder `#{toFilePath dir}'.|]
              Just date ->
                let vals = allValues (mapBlog @blogspec (\f -> nameF f dir) (\c -> contentF c dir) sub)
                in foldl' (\t (path, val) -> insert path (addDate date val) t) mempty vals
        mappings = foldMapFoldersHere toMappings $
          filterFoldersHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left
                [i|I was looking for a directory matching `#{symbolVal @selector Proxy}', but I couldn't find one.|]
         else mappings
    where toDate dir = parseTimeM False defaultTimeLocale "%Y-%m-%d" $
            take 10 (toFilePath dir)
          addDate date = (fmap . fmap) $ \(ofile, meta, prodF) ->
            (ofile, JSON.insert "date" (Date date) meta, prodF)

instance KnownSymbol selector
    => HasMapping (DATE (FILE selector)) where
  type NameMapper (DATE (FILE selector)) = NameMapper (FILE selector)
  type ContentMapper (DATE (FILE selector)) m = ContentMapper (FILE selector) m

  mapBlog nameF contentF fs =
    let glob = Glob.parse (symbolVal @selector Proxy)
        toEntry f = singletonValue $ case toDate f of
          Nothing -> validationNel $ Left
            [i|Couldn't parse a valid date from filename `#{toFilePath f}'.|]
          Just date ->
            let (oname, val) = nameF f
            in validationNel $ Right $ FileData
                 (f, (oname, JSON.union (JSON.singleton "date" (Date date)) val, contentF))
        mappings = foldMapValuesHere toEntry $
          filterValuesHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left
                [i|I was looking for a file matching `#{symbolVal @selector Proxy}', but I couldn't find one.|]
         else mappings
    where toDate file = parseTimeM False defaultTimeLocale "%Y-%m-%d" $
            take 10 (toFilePath file)
