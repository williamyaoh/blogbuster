{-# LANGUAGE AllowAmbiguousTypes  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveFunctor        #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE QuasiQuotes          #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeApplications     #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}

module Blogbuster.Mapping where

import Path

import Data.ByteString         ( ByteString )
import Data.Foldable           ( foldl' )
import Data.Function           ( on )
import Data.Monoid             ( Alt(..) )
import Data.Proxy              ( Proxy(..) )
import Data.String.Interpolate ( i, __i )
import Data.Validation         ( Validation(..), validationNel )

import Control.Monad ( ap )

import Data.Kind    ( Type )
import GHC.TypeLits ( KnownSymbol, Symbol, symbolVal )

import qualified Glob
import           TreeStore

import Blogbuster.Output    ( OutputTree )
import Blogbuster.Meta      ( Value )
import Blogbuster.Primitive
import Blogbuster.FileData

class SymbolList (l :: [Symbol]) where
  symbols :: [String]

instance SymbolList '[] where
  symbols = []
instance (KnownSymbol sym, SymbolList syms) => SymbolList (sym ': syms) where
  symbols = symbolVal @sym Proxy : symbols @syms

-- Eventually, to simplify the implementation of adding attributes in our
-- production functions, we'll probably want to create a new wrapping monad
-- that allows for a `setAttr' function or similar, and change the name/content
-- functions to use it. That way we don't foist the burden of constructing
-- the output meta on our production functions.

class HasMapping (blogspec :: Type) where
  type NameMapper blogspec :: Type
  type ContentMapper blogspec (m :: Type -> Type) :: Type

  mapBlog :: (Path Rel File -> NameMapper blogspec)
          -> (ByteString -> ContentMapper blogspec m)
          -> Filesystem
          -> OutputTree m

-- Funnily enough, though we've factored out the filtering and traversing
-- aspects, we're left wanting to factor out the common cruft of checking
-- if there are any matches and adding an error message...

instance KnownSymbol selector => HasMapping (FILE selector) where
  type NameMapper (FILE selector) = (Path Rel File, Value)
  type ContentMapper (FILE selector) m = m (ByteString, Value)

  mapBlog nameF contentF fs =
    let glob = Glob.parse (symbolVal @selector Proxy)
        toEntry f =
          let (oname, meta) = nameF f
          in singletonValue
               (validationNel $ Right $ FileData (f, (oname, meta, contentF)))
        mappings = foldMapValuesHere toEntry $
          filterValuesHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left
                [i|I was looking for a file matching `#{symbolVal @selector Proxy}', but I couldn't find one.|]
         else mappings

instance SymbolList selectors => HasMapping (FILES selectors) where
  type NameMapper (FILES selectors) = (Path Rel File, Value)
  type ContentMapper (FILES selectors) m = m (ByteString, Value)

  mapBlog nameF contentF fs =
    let globs = fmap Glob.parse (symbols @selectors)
        glob = foldl' Glob.Union Glob.Null globs
        toEntry f =
          let (oname, meta) = nameF f
          in singletonValue $ validationNel $ Right $
               FileData (f, (oname, meta, contentF))
        mappings = foldMapValuesHere toEntry $
          filterValuesHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left [__i|
           I was looking for a file matching any of
             #{symbols @selectors}
           but I couldn't find one.
         |]
         else mappings

instance (HasMapping blogspec, KnownSymbol selector)
    => HasMapping ((selector :: Symbol) :/ blogspec) where
  type NameMapper (selector :/ blogspec) = NameMapper blogspec
  type ContentMapper (selector :/ blogspec) m = ContentMapper blogspec m

  mapBlog nameF contentF fs =
    let glob = Glob.parse (symbolVal @selector Proxy)
        toMappings dir sub =
          singletonFolder dir (mapBlog @blogspec nameF contentF sub)
        mappings = foldMapFoldersHere toMappings $
          filterFoldersHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left
                [i|I was looking for a directory matching `#{symbolVal @selector Proxy}', but I couldn't find one.|]
         else mappings

instance (HasMapping blogspec, KnownSymbol selector)
    => HasMapping (DIR selector :/ blogspec) where
  type NameMapper (DIR selector :/ blogspec) = Path Rel Dir -> NameMapper blogspec
  type ContentMapper (DIR selector :/ blogspec) m = Path Rel Dir -> ContentMapper blogspec m

  mapBlog nameF contentF fs =
    let glob = Glob.parse (symbolVal @selector Proxy)
        toMappings dir sub =
          singletonFolder dir $
            mapBlog @blogspec (\f -> nameF f dir) (\c -> contentF c dir) sub
        mappings = foldMapFoldersHere toMappings $
          filterFoldersHere (Glob.match glob . toFilePath) fs
    in if TreeStore.null mappings
         then singletonValue $ validationNel $ Left
                [i|I was looking for a directory matching `#{symbolVal @selector Proxy}', but I couldn't find one.|]
         else mappings

-- For this implementation we're just going to check if the subcomponent's
-- output tree contains only errors at the current level. I'm not really
-- happy with defining it that way because it feels imprecise, but for now
-- it should work.
instance HasMapping blogspec => HasMapping (OPT blogspec) where
  type NameMapper (OPT blogspec) = NameMapper blogspec
  type ContentMapper (OPT blogspec) m = ContentMapper blogspec m

  mapBlog nameF contentF fs =
    let output = mapBlog @blogspec nameF contentF fs
        vals = valuesHere output
    in if all isError vals && not (Prelude.null vals)
         then TreeStore.empty
         else output
    where isError (Failure _) = True
          isError (Success _) = False

instance (HasMapping spec1, HasMapping spec2) => HasMapping (spec1 :+ spec2) where
  type NameMapper (spec1 :+ spec2) = NameMapper spec1 :+ NameMapper spec2
  type ContentMapper (spec1 :+ spec2) m = ContentMapper spec1 m :+ ContentMapper spec2 m

  mapBlog mapF contentF fs =
    let lnameF    = \file    -> case mapF file of { f :+ _ -> f }
        lcontentF = \content -> case contentF content of { f :+ _ -> f }
        rnameF    = \file    -> case mapF file of { _ :+ f -> f }
        rcontentF = \content -> case contentF content of { _ :+ f -> f }
    in mapBlog @spec1 lnameF lcontentF fs <> mapBlog @spec2 rnameF rcontentF fs

----------------------------------------
-- A convenient monadic interface for providing production functions
----------------------------------------

data Production' blogspec m = Production
  { prodNameF    :: Maybe (Path Rel File -> NameMapper blogspec)
  , prodContentF :: Maybe (ByteString -> ContentMapper blogspec m)
  }
newtype Production blogspec m a = P (Production' blogspec m, a)
  deriving Functor

instance Applicative (Production blogspec m) where
  pure x = P (Production Nothing Nothing, x)
  (<*>) = ap

instance Monad (Production blogspec m) where
  return = pure
  (>>=) (P (Production mnameF1 mcontentF1, x)) f = case f x of
    P (Production mnameF2 mcontentF2, y) ->
      P ( Production (getAlt $ on (<>) Alt mnameF1 mnameF2)
                     (getAlt $ on (<>) Alt mcontentF1 mcontentF2)
        , y
        )

name :: (Path Rel File -> NameMapper blogspec) -> Production blogspec m ()
name nameF = P (Production (Just nameF) Nothing, ())

content :: (ByteString -> ContentMapper blogspec m) -> Production blogspec m ()
content contentF = P (Production Nothing (Just contentF), ())

normalizeProduction :: forall blogspec m. HasMapping blogspec
                    => Production' blogspec m
                    -> Filesystem
                    -> OutputTree m
normalizeProduction (Production mnameF mcontentF) fs =
  case (mnameF, mcontentF) of
    (Just nameF, Just contentF) -> mapBlog @blogspec nameF contentF fs
    (Nothing, Nothing) -> singletonValue $ validationNel $
      Left "missing name mapping function and content mapping function"
    (Nothing, _) -> singletonValue $ validationNel $
      Left "missing name mapping function"
    (_, Nothing) -> singletonValue $ validationNel $
      Left "missing content mapping function"

-- Yeah, changing our typeclass to a continuation-based approach, both for
-- registering successful values, and for reporting failures, sounds like
-- a pretty good deal right about now, doesn't it? Because if you look
-- at our desired behavior for normalizeProduction, it should actually
-- show every file that *might* have been produced, but wasn't because
-- a correct production function wasn't provided to us. The way it's working
-- right now, the way we have to write it, makes it so that we can only
-- report that once, at the top level, and without any information about
-- which production it's coming from. Although I wonder if that would
-- get caught by optional, that approach... who knows. there's probably
-- some way to order it such that that doesn't happen.
