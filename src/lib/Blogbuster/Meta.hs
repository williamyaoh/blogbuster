{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns      #-}

module Blogbuster.Meta where

import Control.Monad                     ( ap )
import Control.Monad.Trans.Writer.Strict ( Writer, execWriter, tell )
import Control.Exception ( IOException, try )

import Path

import qualified Data.Aeson          as JSON
import qualified Data.Aeson.KeyMap   as JSON
import           Data.Foldable
import           Data.Text           ( Text )
import qualified Data.Text           as T
import qualified Data.Text.IO        as T
import qualified Data.Text.Lazy      as T hiding ( pack )
import           Data.Time
import           Data.Vector         ( Vector )
import Data.Maybe ( mapMaybe, listToMaybe )
import Data.List ( sortBy )
import Data.Function ( on )

import qualified Text.Mustache as Stache

import Blogbuster.FileData
import TreeStore

-- |
-- Top-level dictionaries that we can associate with each file.
type Value = JSON.KeyMap Node

-- |
-- Essentially just a JSON value, but augmented with some extra cases for
-- useful data that often comes up in case of blogs.
data Node
  = Number Double
  | Boolean Bool
  | String Text
  | URI (Path Rel File)
  | Date Day
  | Time TimeOfDay
  | Timestamp UTCTime
  | Array (Vector Node)
  | Object (JSON.KeyMap Node)
  deriving (Eq, Show)

type RenderNode = Node -> JSON.Value

defaultRender :: RenderNode
defaultRender node = case node of
  Number d -> JSON.Number (fromRational $ toRational d)
  Boolean b -> JSON.Bool b
  String t -> JSON.String t
  URI p -> JSON.String $ T.pack $ toFilePath p
  Date d -> JSON.String $ T.pack $ formatTime defaultTimeLocale "%Y-%m-%d" d
  Time t -> JSON.String $ T.pack $ formatTime defaultTimeLocale "%H:%M" t
  Timestamp t ->
    JSON.String $ T.pack $ formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%SZ" t
  Array a -> JSON.Array $ fmap defaultRender a
  Object o -> JSON.Object $ fmap defaultRender o

data AttrF a
  = Pure a
  | FileDep (Path Rel File) (Value -> AttrF a)
  | DirDep (Path Rel Dir) ([Value] -> AttrF a)
  deriving Functor

instance Applicative AttrF where
  pure = Pure
  (<*>) = ap

instance Monad AttrF where
  return = pure
  (>>=) m f = joinAttrF (fmap f m)

joinAttrF :: AttrF (AttrF a) -> AttrF a
joinAttrF (Pure f)         = f
joinAttrF (FileDep path f) = FileDep path (fmap joinAttrF f)
joinAttrF (DirDep path f)  = DirDep path (fmap joinAttrF f)

data AttrSpecifier = All | FileSpec (Path Rel File)
  deriving (Eq, Ord)
data AttrEntry = AttrEntry
  { attrSpec  :: AttrSpecifier
  , attrFuncs :: [Value -> AttrF Value]
  }
newtype Attr a = Attr
  { getAttr :: ([TreeStore (Path Rel Dir) AttrEntry], a) }
  deriving Functor

instance Eq AttrEntry where
  (==) entry1 entry2 = attrSpec entry1 == attrSpec entry2
instance Ord AttrEntry where
  compare entry1 entry2 = attrSpec entry1 `compare` attrSpec entry2

instance Applicative Attr where
  pure x = Attr { getAttr = ([], x) }
  (<*>) = ap

instance Monad Attr where
  return = pure
  (>>=) (Attr (mappings1, x)) f = case f x of
    Attr (mappings2, y) ->
      Attr (mappings1 ++ mappings2, y)

-- The way that we're writing our attribute pulling here is rather imprecise.
-- We don't handle things like up-dir directives, in particular. We'd probably
-- want to improve this eventually.

runAttrF :: AttrF a -> TreeStore (Path Rel Dir) (FileData Value) -> a
runAttrF (Pure x) _ = x
runAttrF (FileDep path f) tree =
  let file = filename path
      dirs = splitDirs (parent path)
      here = descends dirs tree
      dat = find (\(FileData (f, _)) -> f == file) (valuesHere here)
  in case dat of
    Just (FileData (_, val)) -> runAttrF (f val) tree
    Nothing                  -> undefined  -- TODO: fix this
runAttrF (DirDep path f) tree =
  let dirs = splitDirs path
      here = descends dirs tree
      attrF = f $ fmap (\(FileData (_, val)) -> val) (valuesHere here)
  in runAttrF attrF tree

calcLayer :: TreeStore (Path Rel Dir) AttrEntry
          -> TreeStore (Path Rel Dir) (FileData Value)
          -> TreeStore (Path Rel Dir) (FileData Value)
calcLayer mapTree attrTree =
  let mappings = allValues mapTree
  in foldl' applyEntry attrTree mappings
  where applyEntry tree (path, entry) = case attrSpec entry of
          All           -> goAll tree path (attrFuncs entry)
          FileSpec file -> goFile tree path file (attrFuncs entry)

        goAll !tree _ [] = tree
        goAll !tree path (f:fs) =
          let local = descends path tree
              vals = valuesHere local
              updated = (fmap . fmap) (\val -> runAttrF (f val) tree) vals
              newTree = foldl' (\t newVal -> insert path newVal t) tree updated
          in goAll newTree path fs

        goFile !tree _ _ [] = tree
        goFile !tree path file (f:fs) =
          let local = descends path tree
              mdat = find (\(FileData (file', _)) -> file' == file) (valuesHere local)
          in case mdat of
               Nothing -> tree
               Just dat ->
                 let updated = fmap (\val -> runAttrF (f val) tree) dat
                 in goFile (insert path updated tree) path file fs

-- One small consequence of the way we've define the layer calculation
-- is that any *individual* attr function inside an entry is applied
-- *all at the same time* for functions that affect all metadata within
-- a directory. This seems basically correct to me.

calcAttrs :: Attr a
          -> TreeStore (Path Rel Dir) (FileData Value)
          -> TreeStore (Path Rel Dir) (FileData Value)
calcAttrs (Attr (layers, _)) tree =
  foldl' (\t layer -> calcLayer layer t) tree layers

interpAttrs :: RenderNode
            -> Path rel Dir
            -> TreeStore (Path Rel Dir) (FileData Value)
            -> IO ()
interpAttrs render root tree = do
  let meta = allValues tree
  flip traverse_ meta $ \(path, FileData (file, val)) -> do
    let fullPath = foldl' (</>) root path </> file
    mcontents <- try (T.readFile $ toFilePath fullPath)
    case mcontents of
      Left error -> let _ = error :: IOException in pure ()
      Right contents -> do
        let mtemplate = Stache.compileMustacheText "" contents
        case mtemplate of
          Left error -> fail (show error)  -- TODO: we should probably continue if
                                           -- a single file fails
          Right template -> do
            let json = JSON.Object (fmap render val)
            T.writeFile (toFilePath fullPath) $
              T.toStrict $ Stache.renderMustache template json

-- TODO: Right now we're attempting to run the templating on *all* output
-- files, which isn't what we want. We should eventually change this
-- implementation to only sniff out the files that have been specified through
-- the Attr.

postprocess :: Path Rel Dir
            -> TreeStore (Path Rel Dir) (FileData Value)
            -> Attr a
            -> IO ()
postprocess root attrTree mappings =
  postprocessWith root attrTree defaultRender mappings

postprocessWith :: Path Rel Dir
                -> TreeStore (Path Rel Dir) (FileData Value)
                -> RenderNode
                -> Attr a
                -> IO ()
postprocessWith root attrTree renderF mappings =
  interpAttrs renderF root (calcAttrs mappings attrTree)

layer :: Builder (Path Rel Dir) AttrEntry -> Attr ()
layer builder = Attr ([toTreeStore builder], ())

all :: Writer [Value -> AttrF Value] a -> Builder (Path Rel Dir) AttrEntry
all w = store $ AttrEntry
  { attrSpec = All
  , attrFuncs = execWriter w
  }

file :: Path Rel File
     -> Writer [Value -> AttrF Value] a
     -> Builder (Path Rel Dir) AttrEntry
file path w = store $ AttrEntry
  { attrSpec = FileSpec path
  , attrFuncs = execWriter w
  }

attr :: JSON.Key -> (Value -> AttrF (Maybe Node)) -> Writer [Value -> AttrF Value] ()
attr key f =
  let g value = do
        mresult <- f value
        pure $! case mresult of
          Nothing -> value
          Just result -> JSON.insert key result value
  in tell [g]

reqFile :: Path Rel File -> AttrF Value
reqFile path = FileDep path pure

reqDir :: Path Rel Dir -> AttrF [Value]
reqDir path = DirDep path pure

-- someDef :: Attr ()
-- someDef = do
--   layer $ do
--     folder "posts" $ do
--       all $ do
--        attr "something" ...
--   layer $ do
--     file "index.html" $ do
--       attr "something" ...
--     file "archive.html" $ do
--       attr "something" ...

-- So, as part of this, the absolute simplest attributes that we could
-- calculate are the next and previous post links, something that
-- would've been nearly impossible using Hakyll.

-- Without the things we were talking about (thisDir/here functions), this
-- isn't possible to write in a very general way. Still...

fromJSON :: JSON.Object -> Value
fromJSON = fmap toNode
  where toNode :: JSON.Value -> Node
        toNode (JSON.Null) = undefined  -- TODO: Fix this
        toNode (JSON.Number n) = Number (fromRational $ toRational n)
        toNode (JSON.Bool b) = Boolean b
        toNode (JSON.String s) = String s
        toNode (JSON.Array arr) = Array (fmap toNode arr)
        toNode (JSON.Object obj) = Object (fmap toNode obj)

lookupNumber :: JSON.Key -> Value -> Maybe Double
lookupNumber key val = case JSON.lookup key val of
  Just (Number n) -> pure n
  _otherwise      -> Nothing

lookupBoolean :: JSON.Key -> Value -> Maybe Bool
lookupBoolean key val = case JSON.lookup key val of
  Just (Boolean bool) -> pure bool
  _otherwise          -> Nothing

lookupString :: JSON.Key -> Value -> Maybe Text
lookupString key val = case JSON.lookup key val of
  Just (String str) -> pure str
  _otherwise        -> Nothing

lookupURI :: JSON.Key -> Value -> Maybe (Path Rel File)
lookupURI key val = case JSON.lookup key val of
  Just (URI uri) -> pure uri
  _otherwise     -> Nothing

lookupDate :: JSON.Key -> Value -> Maybe Day
lookupDate key val = case JSON.lookup key val of
  Just (Date day) -> pure day
  _otherwise      -> Nothing

lookupTime :: JSON.Key -> Value -> Maybe TimeOfDay
lookupTime key val = case JSON.lookup key val of
  Just (Time time) -> pure time
  _otherwise       -> Nothing

lookupTimestamp :: JSON.Key -> Value -> Maybe UTCTime
lookupTimestamp key val = case JSON.lookup key val of
  Just (Timestamp ts) -> pure ts
  _otherwise          -> Nothing

lookupArray :: JSON.Key -> Value -> Maybe (Vector Node)
lookupArray key val = case JSON.lookup key val of
  Just (Array arr) -> pure arr
  _otherwise       -> Nothing

lookupObject :: JSON.Key -> Value -> Maybe (JSON.KeyMap Node)
lookupObject key val = case JSON.lookup key val of
  Just (Object obj) -> pure obj
  _otherwise        -> Nothing

calcNextPost :: Path Rel Dir -> Value -> AttrF (Maybe Node)
calcNextPost postDir val = do
  posts <- mapMaybe (\val -> (,) <$> lookupDate "date" val <*> pure val) <$>
    reqDir postDir

  mnext <- pure $! do
    now <- lookupDate "date" val
    here <- lookupURI "uri" val
    let after = sortBy (on compare fst) $
          filter (\(date, _) -> date > now) posts
    (_, nextPost) <- listToMaybe after
    lookupURI "uri" nextPost >>= stripProperPrefix (parent here)

  pure $! URI <$> mnext

calcPrevPost :: Path Rel Dir -> Value -> AttrF (Maybe Node)
calcPrevPost postDir val = do
  posts <- mapMaybe (\val -> (,) <$> lookupDate "date" val <*> pure val) <$>
    reqDir postDir

  mprev <- pure $! do
    now <- lookupDate "date" val
    here <- lookupURI "uri" val
    let before = sortBy (on (flip compare) fst) $
          filter (\(date, _) -> date < now) posts
    (_, prevPost) <- listToMaybe before
    lookupURI "uri" prevPost >>= stripProperPrefix (parent here)

  pure $! URI <$> mprev

addSurroundingPosts :: Path Rel Dir -> Writer [Value -> AttrF Value] ()
addSurroundingPosts postDir = do
  attr "next_post" $ calcNextPost postDir
  attr "prev_post" $ calcPrevPost postDir
