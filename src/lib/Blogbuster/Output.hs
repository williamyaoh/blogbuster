{-# LANGUAGE QuasiQuotes      #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}

module Blogbuster.Output where

import qualified Data.Aeson.KeyMap       as JSON
import           Data.ByteString         ( ByteString )
import qualified Data.ByteString         as BS
import           Data.Fixed              ( Centi )
import           Data.Foldable
import           Data.List.NonEmpty      ( NonEmpty(..) )
import           Data.String.Interpolate ( i )
import           Data.Validation
import           Path
import           Path.IO                 ( ensureDir )

import Control.Exception                ( Exception(..), IOException, try )
import Control.Monad                    ( unless, when )
import Control.Monad.IO.Class
import Control.Monad.Reader.Class
import Control.Monad.State.Class
import Control.Monad.Writer.Class
import Control.Monad.Trans.Class ( lift )
import Control.Monad.Trans.Reader       ( ReaderT, runReaderT )
import Control.Monad.Trans.State.Strict ( StateT, evalStateT )
import Control.Monad.Trans.Writer.Strict ( WriterT, runWriterT, execWriterT )

import System.Clock.TimeIt ( elapsedTime )

import TreeStore hiding ( null )
import Blogbuster.FileData
import Blogbuster.Meta

type OutputEntry m = FileData (Path Rel File, Value, ByteString -> m (ByteString, Value))
type OutputTree m =
  TreeStore
    (Path Rel Dir)
    (Validation (NonEmpty String) (OutputEntry m))

data OutputEnv irel orel m = OutputEnv
  { outputIndent  :: Int
  , outputCwd     :: Path irel Dir
  , outputRootDir :: Path orel Dir
  , outputTrans   :: (forall a. m a -> IO a)
  }

outputPrefix :: OutputEnv irel orel m -> String
outputPrefix env = take (outputIndent env) (repeat ' ')

type Out irel orel m =
  ReaderT (OutputEnv irel orel m)
    (StateT Int
      (WriterT [NonEmpty String]
        (WriterT [([Path Rel Dir], FileData Value)]
          IO)))
    ()

produceOutput :: Path irel Dir -> Path orel Dir -> OutputTree IO -> IO (TreeStore (Path Rel Dir) (FileData Value))
produceOutput = produceOutput' id

produceOutput' :: (forall a. m a -> IO a)
               -> Path irel Dir
               -> Path orel Dir
               -> OutputTree m
               -> IO (TreeStore (Path Rel Dir) (FileData Value))
produceOutput' trans indir outdir tree = do
  let env = OutputEnv 2 indir outdir trans

  putStrLn (toFilePath indir)
  ((errors, metaEntries), elapsed) <- elapsedTime $
    runWriterT $ execWriterT $ flip evalStateT 1 $ flip runReaderT env $
      go tree
  unless (null errors) $ displayErrors errors
  displaySummary elapsed (length $ allValues tree) (length errors)
  pure $! foldl' (\t (path, meta) -> insert path meta t) mempty metaEntries
  where go tree = do
          traverse_ produceEntry $ valuesHere tree
          flip traverse_ (foldersHere tree) $ \dir -> do
            prefix <- asks outputPrefix
            liftIO $ putStr prefix
            liftIO $ putStrLn (toFilePath dir)
            local (\env -> env { outputIndent = outputIndent env + 2, outputCwd = outputCwd env </> dir }) $
              go $ descend dir tree

produceEntry :: Validation (NonEmpty String) (OutputEntry m)
             -> Out irel orel m
produceEntry entry = case entry of
  Failure errors -> produceError errors
  Success file   -> produceFile file

produceError :: NonEmpty String -> Out irel orel m
produceError errors = do
  errno <- get
  put (errno+1)

  prefix <- asks outputPrefix
  liftIO $ putStr prefix
  liftIO $ putStrLn [i|!! [ERROR][#{errno}] Something went wrong, see [#{errno}] below !!|]

  tell [errors]

produceFile :: OutputEntry m -> Out irel orel m
produceFile (FileData (iname, (oname, meta, contentF))) = do
  env <- ask
  prefix <- asks outputPrefix

  let ifull = outputCwd env </> iname
  let ofull = outputRootDir env </> oname

  mresult <- liftIO $ try $ do
    contents <- BS.readFile $ toFilePath ifull
    (output, contentMeta) <- outputTrans env (contentF contents)
    ensureDir (parent ofull)
    BS.writeFile (toFilePath ofull) output
    pure $! contentMeta
  case mresult of
    Left e -> do
      errno <- get
      put (errno+1)

      liftIO $ putStr prefix
      liftIO $ putStrLn [i|!! [ERROR][#{errno}] Something went wrong, see [#{errno}] below !!|]

      tell [(displayException (e :: IOException) :| [])]
    Right contentMeta -> do
      liftIO $ putStr prefix
      liftIO $ putStrLn [i|#{toFilePath iname} → #{toFilePath ofull}|]

      let fullMeta = JSON.union meta contentMeta
      lift $ lift $ lift $
        tell [(opath, FileData (ofilename, JSON.insert "uri" (URI oname) fullMeta))]
  where ofilename = filename oname
        opath = splitDirs $ parent oname

displayErrors :: [NonEmpty String] -> IO ()
displayErrors errors = do
  flip traverse_ (zip [1..] errors) $ \(errno, errs) -> do
    putStrLn ""
    putStrLn [i|[ERROR][#{errno :: Int}]|]
    traverse_ (\s -> putStr "  " >> putStrLn s) errs

displaySummary :: Double -> Int -> Int -> IO ()
displaySummary elapsed total errs = do
  let trunc = fromRational @Centi (toRational elapsed)
  putStrLn ""
  putStr [i|[#{total - errs} file(s) generated in #{trunc}s|]
  when (errs > 0) $ putStr [i|, #{errs} error(s)|]
  putStrLn "]"
