{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeOperators #-}

module FileSelector where

import Freer

import Data.Functor.Compose

import Control.Monad

import System.Directory
import System.FilePath.Glob hiding ( Pattern )

data MatchTarget = File | Directory
  deriving (Eq, Show)

type Pattern = FilePath

data SelectorF a where
  MatchOne :: MatchTarget -> Pattern -> SelectorF FilePath
  MatchMany :: MatchTarget -> [Pattern] -> SelectorF [FilePath]
  Below :: Pattern -> Selector a -> SelectorF (FilePath, a)
  Both :: Selector a -> Selector a -> SelectorF a
type Selector = Freer SelectorF

file :: Pattern -> Selector FilePath
file pat = send (MatchOne File pat)

files :: [Pattern] -> Selector [FilePath]
files pats = send (MatchMany File pats)

dir :: Pattern -> Selector FilePath
dir pat = send (MatchOne Directory pat)

dirs :: [Pattern] -> Selector [FilePath]
dirs pats = send (MatchMany Directory pats)

below :: Pattern -> Selector a -> Selector (FilePath, a)
below pat sel = send (Below pat sel)

both :: Selector a -> Selector a -> Selector a
both l r = send (Both l r)

infixr 8 </>
(</>) :: Pattern -> Selector a -> Selector a
(</>) pat sel = snd <$> below pat sel

infixl 7 .||.
(.||.) :: Selector a -> Selector a -> Selector a
(.||.) = both

-- How do we implement a selector?

-- isPathSeparator, splitPath, normalise

-- For now, we'll assume that our users are giving us each piece as
-- one single path segment, with no directory separators. Later we
-- can add some validation to that.

joinLIO :: (IO `Compose` []) ((IO `Compose` []) a) -> (IO `Compose` []) a
joinLIO body = Compose $ do
  ios <- getCompose body
  mconcat <$> traverse getCompose ios

-- |
-- Give back all the instances that match, using the current directory as
-- the root of the selector.
selectFiles :: Selector a -> (IO `Compose` []) a
selectFiles = \case
  Pure a -> pure a
  Bind (MatchOne File pat) k ->
    joinLIO $ fmap selectFiles k <$>
      (Compose $ join $ filterM doesFileExist <$> glob pat)
  Bind (MatchOne Directory pat) k ->
    joinLIO $ fmap selectFiles k <$>
      (Compose $ join $ filterM doesDirectoryExist <$> glob pat)
  Bind (MatchMany File pats) k ->
    joinLIO $ fmap selectFiles k <$>
      (Compose $ fmap pure $ join $ filterM doesFileExist <$> fmap mconcat (traverse glob pats))
  Bind (MatchMany Directory pats) k ->
    joinLIO $ fmap selectFiles k <$>
      (Compose $ fmap pure $ join $ filterM doesDirectoryExist <$> fmap mconcat (traverse glob pats))
  Bind (Below pat sel) k ->
    joinLIO $ fmap selectFiles k <$>
      joinLIO (Compose $ do
        dirs <- join $ filterM doesDirectoryExist <$> glob pat
        (fmap . fmap) (Compose . pure) $
          traverse (\dir -> (fmap . fmap) ((,) dir) $ withCurrentDirectory dir (getCompose $ selectFiles sel)) dirs)
  Bind (Both l r) k ->
    joinLIO $ fmap selectFiles k <$>
      (Compose $ do
        lresults <- getCompose $ selectFiles l
        rresults <- getCompose $ selectFiles r
        pure $! lresults ++ rresults)

-- Selectors will need some way to fail.
-- I wonder if we want some Servant-style way of specifying our folder structure.

-- - 2020-05-31-some-title/
--   - post.md
--   - img1.png
--   - img2.png
--   - img3.svg

folderPost :: Selector (FilePath, [FilePath])
folderPost = (,) <$> file "post.md" <*> files ["*.png", "*.svg", "*.gif", "*.jpg", "*.jpeg"]
