{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Blog where

import Data.Function ( (&) )
import Data.Functor.Compose
import Control.Monad.IO.Class
import Data.ByteString ( ByteString )
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import Data.Text ( Text )
import Data.Foldable ( traverse_ )
import qualified Data.Digest.Pure.MD5 as MD5
import Data.UUID ( UUID )

import qualified Data.Binary as Bin

import Text.HTML.TagSoup

import Freer
import FileSelector

data BlogPersistF a where
  Emit :: FilePath -> [ByteString] -> BlogPersistF ()
  LoadFile :: FilePath -> BlogPersistF ByteString
type BlogPersist a = Freer BlogPersistF a

-- emit :: FilePath -> [ByteString] -> BlogPersist ()
-- emit path contents = send (Emit path contents)

-- loadFile :: FilePath -> BlogPersist ByteString
-- loadFile = send . LoadFile

emit :: FilePath -> [ByteString] -> IO ()
emit path contents = writeFile path "" >> traverse_ (BS.appendFile path) contents

loadFile :: FilePath -> IO ByteString
loadFile = BS.readFile

runBlogPersistIO :: MonadIO m => BlogPersist a -> m a
runBlogPersistIO = interpret $ \case
  Emit filepath contents -> liftIO $
    writeFile filepath "" >> traverse_ (BS.appendFile filepath) contents
  LoadFile filepath -> liftIO $
    BS.readFile filepath

--------------------

-- And then to actually try this whole thing out... all we want to do is
-- emit a table of contents. Or something... I do kinda want an example of
-- something that can be reused, but frankly this is probably good enough.

-- All we actually need to do is count TagOpen "h1", "h2", etc... right? And build
-- a hierarchy out of them?

data Hierarchy = Hierarchy Text Text [Hierarchy]
  deriving (Eq, Show)
type TOC = [Hierarchy]

data HierarchyZipper = HZ
  [(Text, Text)]  -- ^ the path we took
  [[Hierarchy]]   -- ^ left side
  [[Hierarchy]]   -- ^ right side
  [Hierarchy]     -- ^ below
  deriving (Eq, Show)

root :: TOC -> HierarchyZipper
root = HZ [] [] []

unzipper :: HierarchyZipper -> TOC
unzipper (HZ [] [] [] below) = below
unzipper other = unzipper (up other)

-- Rather than using three separate lists, the obvious choice is to
-- use one single list instead.

up :: HierarchyZipper -> HierarchyZipper
up (HZ ((name, anchor):ps) (l:ls) (r:rs) below) =
  HZ ps ls rs (l ++ [Hierarchy name anchor below] ++ r)
up hz = hz

leftChild :: HierarchyZipper -> HierarchyZipper
leftChild (HZ ps ls rs (Hierarchy name anchor below:cs)) =
  HZ ((name, anchor):ps) ([]:ls) (cs:rs) below
leftChild hz = hz

rightChild :: HierarchyZipper -> HierarchyZipper
rightChild hz@(HZ _ps _ls _rs []) = hz
rightChild (HZ ps ls rs below) =
  let Hierarchy name anchor below' = last below
  in HZ ((name, anchor):ps) (init below:ls) ([]:rs) below'

rightSibling :: HierarchyZipper -> HierarchyZipper
rightSibling (HZ ((name, anchor):ps) (l:ls) ((Hierarchy name' anchor' below':cousins):rs) below) =
  HZ ((name', anchor'):ps) ((l ++ [Hierarchy name anchor below]):ls) (cousins:rs) below'
rightSibling hz = hz

addChild :: Hierarchy -> HierarchyZipper -> HierarchyZipper
addChild child (HZ path ls rs below) = HZ path ls rs (below ++ [child])

parseHierarchy :: [Tag Text] -> TOC
parseHierarchy tags = unzipper (go tags (root []))
  where go :: [Tag Text] -> HierarchyZipper -> HierarchyZipper
        go [] acc = acc
        go t@(TagOpen tag attrs:tags) hz@(HZ path _left _right _below) =
          if not (tag `elem` ["h1", "H1", "h2", "H2", "h3", "H3", "h4", "H4", "h5", "H5", "h6", "H6"])
            then go tags hz
            else if headerBelow tag path
              then case lookup "id" attrs of
                     Nothing -> go tags (rightChild $ addChild (Hierarchy tag "" []) hz)
                     Just hid -> go tags (rightChild $ addChild (Hierarchy tag hid []) hz)
              else go t (up hz)
        go (_:tags) hz = go tags hz

headerBelow :: Text -> [(Text, Text)] -> Bool
headerBelow _ [] = True
headerBelow tag ((tag', _):_) = Text.toLower tag > Text.toLower tag'

testTOC :: TOC
testTOC =
  [ Hierarchy "Something to do" "#blah"
    [ Hierarchy "Oi vey" "#oivey"
      [ Hierarchy "Yay" "#yay" []
      , Hierarchy "Bar" "#bar" []
      ]
    , Hierarchy "Shalom" "#shalom" []
    ]
  , Hierarchy "Foo" "#foo" []
  ]

--------------------

tocToHTML :: TOC -> Text
tocToHTML [] = ""
tocToHTML other = "<ul>" <> go other <> "</ul>"
  where go :: TOC -> Text
        go [] = ""
        go (Hierarchy name anchor below:rest) =
             "<li><a href=\"#" <> anchor <> "\">"
          <> name
          <> "</a>"
          <> tocToHTML below
          <> "</li>"
          <> go rest

--------------------

match :: Selector a -> (a -> IO b) -> IO [b]
match sel body = do
  results <- getCompose $ selectFiles sel
  traverse body results

--------------------

-- |
-- For hashing the names of posts, so that we have a unique-ish hash to put them
-- under in the `img/` folder.
md5String :: String -> UUID
md5String str = Text.pack str
  & Text.encodeUtf8
  & LBS.fromStrict
  & MD5.md5
  & Bin.encode
  & Bin.decode
