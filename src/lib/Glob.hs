module Glob
  ( Glob(..)
  , match, parse
  )
where

data Glob
  = Null
  | EmptyStr
  | Any
  | Singleton Char
  | Kleene Glob
  | Seq Glob Glob
  | Union Glob Glob
  deriving (Eq, Show)

-- |
-- Prune the glob tree to avoid blowing up in size.
compact :: Glob -> Glob
compact (Kleene Null) = EmptyStr
compact (Kleene EmptyStr) = EmptyStr
compact (Seq Null _) = Null
compact (Seq _ Null) = Null
compact (Seq EmptyStr l) = l
compact (Seq l EmptyStr) = l
compact (Union Null l) = l
compact (Union l Null) = l
compact other = other

-- |
-- Derivative of the glob with respect to the empty string.
delta :: Glob -> Glob
delta Null = Null
delta EmptyStr = EmptyStr
delta Any = Null
delta (Singleton _) = Null
delta (Kleene _) = EmptyStr
delta (Seq l r) = compact $ Seq (delta l) (delta r)
delta (Union l r) = compact $ Union (delta l) (delta r)

derivative :: Char -> Glob -> Glob
derivative _ Null = Null
derivative _ EmptyStr = Null
derivative _ Any = EmptyStr
derivative c (Singleton c') =
  if c == c' then EmptyStr else Null
derivative c (Kleene glob) = compact $ Seq (derivative c glob) (Kleene glob)
derivative c (Seq l r) = compact $ Union (compact $ Seq (delta l) (derivative c r)) (compact $ Seq (derivative c l) r)
derivative c (Union l r) = compact $ Union (derivative c l) (derivative c r)

matchEmpty :: Glob -> Bool
matchEmpty Null = False
matchEmpty EmptyStr = True
matchEmpty Any = False
matchEmpty (Singleton _) = False
matchEmpty (Kleene _) = True
matchEmpty (Seq l r) = matchEmpty l && matchEmpty r
matchEmpty (Union l r) = matchEmpty l || matchEmpty r

match :: Glob -> String -> Bool
match glob "" = matchEmpty glob
match glob (c:cs) = match (derivative c glob) cs

parse :: String -> Glob
parse "" = EmptyStr
parse ('*':cs) = Seq (Kleene Any) (parse cs)
parse (c:cs) = Seq (Singleton c) (parse cs)

-- This is not a particularly fast algorithm because of the data structures
-- we're keeping around, but all the same there are some optimizations
-- we can make.

-- For one, we can add a separate constructor for entire strings so that
-- we don't have to have a thunk for each individual character. Another
-- thing we can do is prune EmptyStr and Null constructors when they
-- appear; clearly EmptyStr don't need to be kept around, and Null
-- will short-circuit Seq, and cause one branch of a Union to fall off.

-- Given how slow this seems to be in fooling around in the REPL, improving
-- the complexity of this might end up being a much higher priority...
