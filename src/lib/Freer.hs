{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}

module Freer where

import Control.Monad ( (>=>) )

data Freer f a where
  Pure :: a -> Freer f a
  Bind :: f a -> (a -> Freer f b) -> Freer f b

instance Functor (Freer f) where
  fmap f (Pure a) = Pure (f a)
  fmap f (Bind fa g) = Bind fa ((fmap . fmap) f g)

instance Applicative (Freer f) where
  pure = Pure
  (<*>) (Pure f) x = fmap f x
  (<*>) (Bind fa g) x = Bind fa (fmap (<*> x) g)

instance Monad (Freer f) where
  return = pure
  (>>=) (Pure a) f = f a
  (>>=) (Bind fa g) f = Bind fa (g >=> f)

send :: f a -> Freer f a
send fa = Bind fa pure

interpret :: Monad m => (forall a. f a -> m a) -> Freer f b -> m b
interpret _ (Pure x) = pure x
interpret natTrans (Bind fa g) =
  natTrans fa >>= fmap (interpret natTrans) g
