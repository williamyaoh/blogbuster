{-# LANGUAGE DeriveFunctor #-}

module TreeStore
  ( TreeStore(..)
  , empty, singletonFolder, singletonValue
  , TreeStore.null
  , insert, insertFolder, descend, descends
  , foldersHere, valuesHere, allValues
  -- * Folds/filters
  , filterValuesHere, foldMapValuesHere
  , filterFoldersHere, foldMapFoldersHere
  -- * Conveniently constructing TreeStores
  , Builder, Builder'(..)
  , toTreeStore, folder, store
  -- * Working with actual filesystem trees
  , Filesystem
  , readFilesystem
  )
where

import           Data.Foldable
import           Data.Map.Strict ( Map )
import qualified Data.Map.Strict as Map
import           Data.Set        ( Set )
import qualified Data.Set        as Set

import Path
import Path.IO

import Control.Arrow          ( second )
import Control.Monad          ( ap )
import Control.Monad.IO.Class

-- |
-- A TreeStore is a hierarchical container of values `v', indexed by a path
-- consisting of a sequence of `k's. Intuitively, you can think of a TreeStore
-- as being similar to a filesystem tree, but in memory and allowing you to
-- use both paths and values of types other than directories and files.
--
-- Many of the intuitions you would expect from a filesystem apply to a
-- TreeStore. Stored values are unique within their level in the hierarchy,
-- and the so are path keys.
newtype TreeStore k v =
  TreeStore ( Set v, Map k (TreeStore k v) )

instance (Show k, Ord k, Show v, Ord v) => Show (TreeStore k v) where
  show = go 0
    where go indent (TreeStore (vals, paths)) =
            let prefix = if indent > 0
                  then take (indent-4) (repeat ' ') <> "└── "
                  else ""
            in (flip foldMap vals $ \val -> prefix <> show val <> "\n")
                 <>
               (flip foldMap (Map.toAscList paths) $ \(path, store) ->
                  prefix <> show path <> "\n" <>
                    go (indent+4) store)

instance (Ord k, Ord v) => Semigroup (TreeStore k v) where
  (<>) (TreeStore (vals1, paths1)) (TreeStore (vals2, paths2)) =
    TreeStore (Set.union vals1 vals2, Map.unionWith (<>) paths1 paths2)

instance (Ord k, Ord v) => Monoid (TreeStore k v) where
  mempty = TreeStore (Set.empty, Map.empty)
  mappend = (<>)

instance Foldable (TreeStore k) where
  foldMap f (TreeStore (vals, paths)) =
    foldMap f vals <> foldMap (foldMap f) (Map.elems paths)

empty :: TreeStore k v
empty = TreeStore (Set.empty, Map.empty)

singletonFolder :: k -> TreeStore k v -> TreeStore k v
singletonFolder k sub = TreeStore (Set.empty, Map.singleton k sub)

singletonValue :: v -> TreeStore k v
singletonValue v = TreeStore (Set.singleton v, Map.empty)

null :: TreeStore k v -> Bool
null (TreeStore (vals, paths)) = Set.null vals && Map.null paths

insert :: (Ord k, Ord v) => [k] -> v -> TreeStore k v -> TreeStore k v
insert [] v (TreeStore (vals, paths)) = TreeStore (Set.insert v vals, paths)
insert (k:ks) v store@(TreeStore (vals, paths)) =
  let below = if Map.member k paths then descend k store else empty
  in TreeStore (vals, Map.insert k (insert ks v below) paths)

insertFolder :: Ord k
             => [k]
             -> k
             -> TreeStore k v
             -> TreeStore k v
             -> TreeStore k v
insertFolder [] name contents (TreeStore (vals, paths)) =
  TreeStore (vals, Map.insert name contents paths)
insertFolder (k:ks) name contents store@(TreeStore (vals, paths)) =
  TreeStore (vals, Map.insert k (insertFolder ks name contents $ descend k store) paths)

descend :: Ord k => k -> TreeStore k v -> TreeStore k v
descend k store@(TreeStore (_, paths)) =
  case Map.lookup k paths of
    Nothing     -> store
    Just store' -> store'

descends :: Ord k => [k] -> TreeStore k v -> TreeStore k v
descends ks store = foldl' (flip descend) store ks

-- |
-- Get the subfolders stored at the current folder in the tree.
foldersHere :: TreeStore k v -> [k]
foldersHere (TreeStore (_, paths)) = Map.keys paths

-- |
-- Get the values stored at the current folder in the tree.
valuesHere :: TreeStore k v -> [v]
valuesHere (TreeStore (vals, _)) = Set.toAscList vals

-- |
-- Get all values in the tree, prefixed with their retrieval path.
allValues :: TreeStore k v -> [([k], v)]
allValues = go []
  where go cwd (TreeStore (vals, paths)) =
          let prefixed = foldl' (\p v -> (reverse cwd, v):p) [] vals
          in prefixed ++ foldMap (\(p, s) -> go (p:cwd) s) (Map.toAscList paths)

filterValuesHere :: Ord v => (v -> Bool) -> TreeStore k v -> TreeStore k v
filterValuesHere pred (TreeStore (vals, paths)) =
  TreeStore (Set.filter pred vals, paths)

foldMapValuesHere :: (Monoid a, Ord v) => (v -> a) -> TreeStore k v -> a
foldMapValuesHere f (TreeStore (vals, _)) =
  foldMap f vals

filterFoldersHere :: Ord k => (k -> Bool) -> TreeStore k v -> TreeStore k v
filterFoldersHere pred (TreeStore (vals, paths)) =
  TreeStore (vals, Map.filterWithKey (\k _ -> pred k) paths)

-- |
-- Passes the mapping function the contents of the subfolder as well.
foldMapFoldersHere :: (Monoid a, Ord k)
                   => (k -> TreeStore k v -> a)
                   -> TreeStore k v
                   -> a
foldMapFoldersHere f (TreeStore (_, paths)) =
  Map.foldlWithKey' (\acc k sub -> acc <> f k sub) mempty paths

-- We can already define a whole bunch of properties that come from
-- the interations of all the functions we've defined so far. But
-- actually testing all of them may end up slowing us down at this point.

-- One weird consequence of defining our signatures like this is that
-- we don't have any way of signalling when we fail to find a given directory,
-- and what to do in that situation. Maybe we should add functions for that
-- case, huh...

newtype Builder' k v a = Builder { runBuilder :: (TreeStore k v, a) }
  deriving Functor
type Builder k v = Builder' k v ()

instance (Ord k, Ord v) => Applicative (Builder' k v) where
  pure x = Builder (mempty, x)
  (<*>) = ap

instance (Ord k, Ord v) => Monad (Builder' k v) where
  return = pure
  (>>=) x f =
    let (store1, (store2, y)) = second runBuilder $ runBuilder $ fmap f x
    in Builder (store1 <> store2, y)

folder :: k -> Builder' k v a -> Builder' k v a
folder k (Builder (store, x)) =
  Builder (TreeStore (Set.empty, Map.singleton k store), x)

store :: v -> Builder' k v ()
store x = Builder (TreeStore (Set.singleton x, Map.empty), ())

toTreeStore :: Builder' k v a -> TreeStore k v
toTreeStore = fst . runBuilder

-- We'll eventually want to define a zipper representation for this as well,
-- for usecases where we actually need to go up the hierarchy, not just down.

type Filesystem = TreeStore (Path Rel Dir) (Path Rel File)

readFilesystem :: MonadIO m
               => Path rel Dir
               -> m Filesystem
readFilesystem path = do
  (dirpaths, files) <- listDirRel path
  dirs <- mapM (go path) dirpaths
  pure $! TreeStore (Set.fromList files, Map.fromList dirs)
  where go prefix dirpath = (,)
          <$> pure dirpath
          <*> readFilesystem (prefix </> dirpath)
