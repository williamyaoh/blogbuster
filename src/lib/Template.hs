{-# LANGUAGE OverloadedStrings #-}

module Template where

import Data.Foldable as F
import Data.Char ( isAlphaNum )
import Data.Text as T
import Data.ByteString as B
import Data.HashMap.Strict as Map
import Data.Attoparsec.ByteString

type DList a = [a] -> [a]

-- Okay. So we still have a few interesting things that we need to do here...
-- this is probably the most sane place to add in the templating "functions"
-- as well. The difference is that they're not going to be defined in...
-- maybe it would be okay to have the user pass in the has of templating
-- functions, for now? Obviously later we'd prefer not to have that be
-- a thing that actually needs to be done... I can start to see why Hakyll
-- didn't provide this functionality by default, since there's no "default"
-- Hakyll site directory structure. But it's kind of inexcusable not to
-- have some sort of functionality here...
--
-- Regardless of that... we'll go with the "map" of templating functions for
-- now. Legit, the templating functions could just be a call to runTemplate, lol.
--
-- Then we just have a few questions... the templates can reuse the same map
-- of replacements that we passed into the initial templating. That's fine.
-- The actual question is how we work with conditionals and so on. And for
-- that, it almost feels like a better thing to do is to use Haskell functions
-- instead of creating some sort of special language syntax! I mean, we could...
-- but I really don't want to. And we're just trying to get something that works
-- as fast as we possibly can.
--
-- So knowing all this... we basically just make our templating function type,
-- ByteString -> ByteString? Wait... I think it needs to get the entire
-- map of... ... ... huh?

-- The basic function we're looking to write is...

runTemplateIO :: HashMap Text ByteString -> FilePath -> IO ByteString
runTemplateIO replacements fp = do
  templateContents <- B.readFile fp
  pure (runTemplate replacements templateContents)

runTemplate :: HashMap Text ByteString -> ByteString -> ByteString
runTemplate replacements body =
  case parseOnly (documentDLP replacements <* endOfInput) body of
    Left parseErr -> error parseErr
    Right f -> mconcat $ f []

documentDLP :: HashMap Text ByteString -> Parser (DList ByteString)
documentDLP replacements = F.foldl' (.) id <$>
  many' (choice [ nonInterpDLP, interpDLP replacements ])

-- ...with perhaps some extra wrapping around this where we can pass in Show
-- instances, where we work with some kind of specific Template datatype
-- rather than raw bytestrings, and so on...

nonInterpDLP :: Parser (DList ByteString)
nonInterpDLP = (:) <$> nonInterpP

interpDLP :: HashMap Text ByteString -> Parser (DList ByteString)
interpDLP replacements = do
  (raw, interp) <- match interpSpliceP
  case Map.lookup interp replacements of
    Nothing -> pure ((:) raw)
    Just replacement -> pure ((:) replacement)

nonInterpP :: Parser ByteString
nonInterpP = do
  w1 <- satisfy (not . (== 0x23))
  before <- takeTill (== 0x23)
  choice
    [ do pound <- string "#"
         word <- satisfy (not . (== 0x7B))
         rest <- nonInterpP
         pure $! (B.singleton w1 <> before <> pound <> B.singleton word <> rest)
    , pure $! B.singleton w1 <> before
    ]

interpSpliceP :: Parser Text
interpSpliceP = string "#{" *> identifierP <* string "}"

identifierP :: Parser Text
identifierP = T.pack <$> many1 idCharP

idCharP :: Parser Char
idCharP = satisfyWith (toEnum . fromIntegral) (\c -> isAlphaNum c || c == '-')
