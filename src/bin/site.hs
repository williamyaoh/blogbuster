{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedLists #-}

module Main where

import Data.Maybe ( catMaybes, listToMaybe )
import Data.Function ( on )
import Data.List ( sortBy )
import Data.Foldable ( foldl' )
import Data.ByteString hiding ( foldl', take )
import Data.ByteString.Search
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Aeson ( Value )
import Data.Aeson.Lens
import Data.Yaml ( decodeEither', prettyPrintParseException )
import Data.Time
import Data.Time.Format.ISO8601

import qualified Text.Pandoc as Pandoc
import qualified Text.Blaze.Renderer.Utf8 as Blaze

import Control.Lens hiding ( below )

import System.Directory
import System.FilePath hiding ( (</>) )
import qualified System.FilePath as FP

import FileSelector
import Section
import Template
import qualified Footnotes
import Blog

-- Okay. So before we can actually switch over our existing site to use this,
-- what do we need to add as... extremely hacky versions of other stuff?
--
-- [x] Pandoc compilation
-- [x] Easy application of templating (pass in some kind of map of contents)
-- [x] Compilation of footnotes between body and footnotes section
-- [x] Addition of title/some sort of simple metadata parser, ideally that
--     interacts with our templates (but doesn't really need to)
-- [x] Pulling of *all* posts for the generation of our archives (we can probably
--     pull this off with our returns from `match`)
-- [x] Date parsing of titles so that we can sort stuff
-- [x] Hash generation for img/static files so that we can put it all in
--     a single img/ directory, and then updating any links to said images
--     within the corresponding post...
-- [x] Oh boy, rendering of Atom feeds... hopefully we can easily generate this from
--     our list of posts. Worst comes to worst, we'll reuse the code from Hakyll
--     and copy/paste it. This might end up being the most annoying part of
--     this whole process. Hopefully not though.
-- [x] Templating functions that can do conditional compilation. Also going to
--     be somewhat complicated, I imagine. We need this for our tailored CTAs.
-- [x] Fix the emitting of files so that it removes the contents first, lol.

data Post = Post
  { postDate :: Day
  , postTitle :: T.Text
  , postFinalURL :: T.Text
  }
  deriving (Eq, Show)

main :: IO ()
main = do
  createDirectoryIfMissing True "_sitebuild"
  createDirectoryIfMissing True "_sitebuild/posts"
  createDirectoryIfMissing True "_sitebuild/img"

  posts <- fmap catMaybes $ match ("site" </> "posts" </> "*" `below` folderPost) $
    \(postfname, (mdfile, imgs)) -> do
      contents <- loadFile mdfile
      case validateContents contents post of
        Left _error -> pure Nothing
        Right (body, meta, footnotes) -> do
          let outputName = (takeFileName postfname) -<.> "html"
          let imgPrefix = show (md5String outputName)
          imgRemapping <- flip traverse imgs $ \img -> do
             imgContents <- loadFile img
             emit ("_sitebuild/img/" FP.</> (imgPrefix ++ "-" ++ takeFileName img)) [imgContents]
             pure $! (takeFileName img, "/img/" FP.</> (imgPrefix ++ "-" ++ takeFileName img))

          let imgFolder body (relImg, absImg) = toStrict $
                replace (encodeStringUTF8 relImg) (encodeStringUTF8 absImg) body

          body <- pure $! foldl' imgFolder body imgRemapping

          body <- markdownToHTML5 body
          footnotes <- traverse markdownToHTML5 footnotes
          (body, footnotes) <- pure $!
            maybe (body, "") (\f -> Footnotes.addLinks (body, f)) footnotes

          meta <- pure $! decodeEither' @Value meta
          title <- case meta of
            Left parseExc -> pure $! T.pack (prettyPrintParseException parseExc)
            Right obj -> pure $! obj ^. key "title"._String

          whichCTA <- case meta of
            Left parseExc -> pure $! T.pack (prettyPrintParseException parseExc)
            Right obj -> pure $! obj ^. key "cta"._String

          let cta = case whichCTA of
                "intermediate" -> "<mailchimp stuff intermediate/>"
                _              -> "<mailchimp stuff normal/>"

          withPost <- flip runTemplateIO "site/templates/post.html"
                        [ ("title", T.encodeUtf8 title)
                        , ("body", body)
                        , ("cta", cta)
                        , ("footnotes", footnotes)
                        ]
          withFraming <- runTemplateIO [ ("body", withPost) ] "site/templates/framing.html"

          emit ("_sitebuild/posts" FP.</> outputName)
            [ withFraming
            ]

          date <- parseTimeM False defaultTimeLocale "%Y-%m-%d" (take 10 $ takeFileName postfname)

          pure $! Just $ Post
            { postTitle = title
            , postDate = date
            , postFinalURL = T.pack ("/posts" FP.</> outputName)
            }

  posts <- pure $! Prelude.reverse (sortBy (on compare postDate) posts)

  do entries <- flip traverse posts $
                  \p -> flip runTemplateIO "site/templates/entry.xml"
                    [ ("title", T.encodeUtf8 (postTitle p))
                    , ("link", T.encodeUtf8 ("https://www.williamyaoh.com" <> postFinalURL p))
                    , ("published", formatPostDate p)
                    ]
     feed <- flip runTemplateIO "site/templates/feed.xml"
               [ ("title", "William Yao's Haskell Musings")
               , ("feedlink", "https://www.williamyaoh.com/feed.atom")
               , ("rootlink", "https://www.williamyaoh.com")
               , ("author", "William Yao")
               , ("email", "williamyaoh@gmail.com")
               , ("lastupdated", maybe "" formatPostDate (listToMaybe posts))
               , ("body", foldl' (\l r -> l <> r <> "\n") "" entries)
               ]

     emit ("_sitebuild/feed.atom") [ feed ]

  print posts

  pure ()

post :: SectionParser (ByteString, ByteString, Maybe ByteString)
post = (,,)
  <$> section "body"
  <*> section "meta"
  <*> optionalSection "footnotes"

formatPostDate :: Post -> ByteString
formatPostDate p =
  let time = UTCTime (postDate p) 0
  in encodeStringUTF8 (iso8601Show time)

encodeStringUTF8 :: String -> ByteString
encodeStringUTF8 = T.encodeUtf8 . T.pack

markdownToHTML5 :: ByteString -> IO ByteString
markdownToHTML5 b = do
  let decoded = T.decodeUtf8 b
  html <- Pandoc.runIOorExplode (Pandoc.readMarkdown Pandoc.def decoded >>= Pandoc.writeHtml5 Pandoc.def)
  pure $! toStrict $ Blaze.renderMarkup html
